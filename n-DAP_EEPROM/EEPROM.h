//**********************
// EEPROM.h for TG-LPC11U35-501
//
// (C)Copyright 2014 All rights reserved by Y.Onodera
// http://einstlab.web.fc2.com
//**********************

#ifndef __ROMAPI_11U35_H_
#define __ROMAPI_11U35_H_

#include "mbed.h"

typedef enum {
    CMD_SUCCESS = 0,            // Command is executed successfully.
    INVALID_COMMAND = 1,        // Invalid command.
    SRC_ADDR_ERROR = 2,         // Source address is not on a word boundary.
    DST_ADDR_ERROR = 3,         // Destination address is not on a correct boundary.
    SRC_ADDR_NOT_MAPPED = 4,    // Source address is not mapped in the memory map.
    DST_ADDR_NOT_MAPPED = 5,    // Destination address is not mapped in the memory map.
    COUNT_ERROR = 6,            // Byte count is not multiple of 4 or is not a permitted value.
    INVALID_SECTOR = 7,         // Sector number is invalid.
    SECTOR_NOT_BLANK = 8,       // Sector is not blank.
    SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION = 9,    // Command to prepare sector for write operation was not executed.
    COMPARE_ERROR =10,          // Source and destination data is not same.
    BUSY = 11                   // flash programming hardware interface is busy.
} IAPstatus;


typedef enum {
    Prepare_sector = 50,
    Copy_RAM_flash = 51,
    Erase_sector = 52,
    Blank_check_sector = 53,
    Read_Part_ID = 54,
    Read_Boot_version = 55,
    Compare = 56,
    Reinvoke_ISP = 57,
    Read_UID = 58,
    Erase_page = 59,
    EEPROM_Write = 61,
    EEPROM_Read = 62
} IAPcommand;


#define IAP_LOCATION (0x1FFF1FF1UL)
typedef void (*IAP)(unsigned int[], unsigned int[]);


class EEPROM {
public:
    EEPROM() : iap_entry( reinterpret_cast<IAP>(IAP_LOCATION) ), cclk_kHz( SystemCoreClock / 1000 ) {}

    int write( unsigned int EEPROM_addr, char *RAM_addr, unsigned int n );
    int read( unsigned int EEPROM_addr, char *RAM_addr, unsigned int n );
    int put( unsigned int EEPROM_addr, char data );
    char get( unsigned int EEPROM_addr );

private:
    IAP             iap_entry;
    unsigned int    command[5];
    unsigned int    status[4];
    char            dat[1];
    int             cclk_kHz;
};


#endif /* __ROMAPI_11U35_H_ */