//**********************
// EEPROM.cpp for TG-LPC11U35-501
// TG-LPC11U35-501 has 4K EEPROM.
//
// usage:
// EEPROM eeprom;
// char RAM[100]
// eeprom.write(0, RAM, 100);
// eeprom.read(0, RAM, 100);
// eeprom.put(0, 0x00);
// i=eeprom.get(0);
//
// note:
// valid EEPROM address range is between 0 to 4031
// top 64 bytes are reserved
//
// (C)Copyright 2014 All rights reserved by Y.Onodera
// http://einstlab.web.fc2.com
//**********************
#include "EEPROM.h"

int EEPROM::write( unsigned int EEPROM_addr, char *RAM_addr, unsigned int n ) {
    command[0] = EEPROM_Write;
    command[1] = EEPROM_addr;
    command[2] = (unsigned int)RAM_addr;
    command[3] = n;
    command[4] = cclk_kHz;

    iap_entry( command, status );

    return ( (int)status[0] );
}


int EEPROM::read( unsigned int EEPROM_addr, char *RAM_addr, unsigned int n ) {
    command[0] = EEPROM_Read;
    command[1] = EEPROM_addr;
    command[2] = (unsigned int)RAM_addr;
    command[3] = n;
    command[4] = cclk_kHz;

    iap_entry( command, status );

    return ( (int)status[0] );
}


int EEPROM::put( unsigned int EEPROM_addr, char data ) {
    dat[0]=data;
    command[0] = EEPROM_Write;
    command[1] = EEPROM_addr;
    command[2] = (unsigned int)dat;
    command[3] = 1;
    command[4] = cclk_kHz;

    iap_entry( command, status );

    return ( (int)status[0] );
}


char EEPROM::get( unsigned int EEPROM_addr ) {
    command[0] = EEPROM_Read;
    command[1] = EEPROM_addr;
    command[2] = (unsigned int)dat;
    command[3] = 1;
    command[4] = cclk_kHz;

    iap_entry( command, status );

    return ( dat[0] );
}