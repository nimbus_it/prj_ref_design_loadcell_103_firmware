#include "mbed.h"
#include <math.h> 
#include <string.h>
#include "ADS1231_GP.h"
#include "uTerminal.h"
#include "EEPROM.h"  
#include "MovingAverage.h"

//==============================================================================
// CONFIG



// The expected nominal load of the load cell. 
// This is the value used for calibration. It doesn't have to be the end
// of scale. You may use a load cell rated for 3kg, but calibrate it using a
// 1kg mass. In this case, use 1.0 [kg] here.
#define LOADCELL_CALIB_VALUE 0.1 // kg




// If the output will be read by another software, a simple new-line ending
// may be enough. If humans will read the number in a terminal such as TeraTerm,
// convenient formatting can keep the printed value in the same line.
// If you want simple output:
#define BEGIN_LINE ""
#define LINE_BREAK "\n"

// If you want escape codes to keep the number in the same position:
//#define BEGIN_LINE "\e[2K"
//#define LINE_BREAK "\r"




// To use USB interface:
USBSerial pc(0x1f00, 0x2012, 0x0001, false);

// To use UART interface:
//Serial pc(P0_19, P0_18) // Pins for n-DAP




// CONSTANTS:
#define mg_TO_Kg  0.000001

//==============================================================================

uTerminal terminal(&pc);

EEPROM eeprom; 

DigitalOut led_blu(P0_11);
DigitalOut led_red(P0_20);

 

// ADS123x:
ADS1231_GP sensor(P0_8, P0_7, P0_12); // DOUT, CLK, PDWN


Ticker sensor_timer;  
MovingAverage averager(16);


signed int last_reading = 0; // last raw AD reading from the n-LOADCELL nblock
signed int last_value = 0; // last calculated value in mg
signed int calib_zero = 0; // The raw reading for a zero weight
signed int calib_factor = 1; // The raw reading for LOADCELL_CALIB_VALUE
signed int calib_tare = 0; // reading offset
signed int ival;
float fval;

char sbuf[1024];


// Place a 4-byte value in EEPROM
void PlaceInEeprom(unsigned int addr, unsigned int val) {
    char RAM[4];
    RAM[0] = (val >> 24) & 0xFF;
    RAM[1] = (val >> 16) & 0xFF;
    RAM[2] = (val >>  8) & 0xFF;
    RAM[3] = (val      ) & 0xFF;
    eeprom.write(addr, RAM, 4);
}
// Retrieves a 4-byte value from EEPROM
unsigned int GetFromEeprom(unsigned int addr) {
    char RAM[4];
    unsigned int tmp;

    eeprom.read(addr, RAM, 4);
    tmp = RAM[0];
    tmp = (tmp << 8) | RAM[1];
    tmp = (tmp << 8) | RAM[2];
    tmp = (tmp << 8) | RAM[3];
    return tmp;
}

// Converts a raw AD value into a signed int interpretation
// A custom function is used to accomodate other data formats
// in future implementations
signed int ADToInt(unsigned int val) {
    if (val < 0x00800000) {
        // Positive or zero value
        return val;
    }
    else {
        // Negative value
        return -(0x1000000-val);
    }
}       

int sensor_flag = 0;
int is_moving = 0;


// Asynchronous (non-blocking) via Ticker
void ReadSensors(void) {
    if (sensor.dataAvailable()) {
        ival = ADToInt(sensor.readValue());
        int avg = (int)averager.filter((float)ival);
        
        last_reading = avg;
        
        /** Final value is found based on the equation:
         *   value = (raw_value - zero - tare_offset) / ( reference - zero )
         * Where:
         *   raw_value is the current int reading from the AD
         *   tare_offset is the AD reading when TARE command was last issued
         *   reference is the AD reading when CALIBREF was last issued
         *   zero is the AD reading when CALIBZERO was last issued
         * The result is relative to the calibration mass weight
         * Then
         *   value_kg = value * LOADCELL_CALIB_VALUE
         * converts the value to kg unit
         * (assuming LOADCELL_CALIB_VALUE is given in kg)
         */
        
        // This is ( reference - zero )
        float fac = (calib_factor - calib_zero);
        // This is a factor to convert a relative value into kg
        float value_to_kg = LOADCELL_CALIB_VALUE / fac;
       
        
        // apply zero (offset) first in a separate operation
        // to force the result into float
        fval = (last_reading - calib_zero - calib_tare);
        // apply factor
        fval *= value_to_kg;
        // fval now is in kg units
        
        // last_value format is int so to keep good resolution we use mg unit
        last_value = rint(fval*1000000); // in mg
        
        // if the average is too different from the value, 
        // the reading is not stable (it's still moving)
        float value_delta = abs(ival - avg); // unit is AD unit
        value_delta *= value_to_kg; // unit is kg
        
        // Is the value stable?
        is_moving = (value_delta > 0.0001)? 1 : 0; // is_moving is 1 if delta > 0.1g
        
        
        // flag to signal we have a new value from the sensor
        sensor_flag = 1;
            
    }
    // else no wait, just keep previous values
}       



void SendOK() {
    terminal.print(BEGIN_LINE);
    terminal.print("OK");
    terminal.print(LINE_BREAK);
}

void SendError() {
    terminal.print(BEGIN_LINE);
    terminal.print("ERROR (");
    terminal.print(terminal.Command);
    terminal.print(" = ");
    terminal.print(terminal.Value);
    terminal.print(")");
    terminal.print(LINE_BREAK);
}

// Toggle a LED when a message is received.
// Used for debugging purposes
int msg_recv = 0;
void msg_received() {
    led_red = !led_red;
    msg_recv = 1;
}

int i;

int main(void) {
    int was_moving = 1; // flag to track changes in is_moving

    // LED flashing on power-up
    for (i=0; i<5; i++) {
        led_blu = 0;
        led_red = 0;
        wait(0.1);
        led_blu = 1;
        led_red = 1;
        wait(0.1);
    }

    // read calibration from EEPROM
    calib_zero = GetFromEeprom(0);
    calib_factor = GetFromEeprom(4);
    calib_tare = GetFromEeprom(8);

    // Turns the AD converter ON
    sensor.setPower(1);

    // Must be faster than AD converter to avoid sample loss.
    // AD is 80 samples/s, so we have 100 Hz
    sensor_timer.attach(&ReadSensors, 0.01);
    
    // Check API below on how to use uTerminal
    // https://os.mbed.com/users/fbcosentino/code/uTerminal/docs/tip/classuTerminal.html
    terminal.attach(&msg_received);
    terminal.ModeAuto();

    // Main loop
    while (1) {
        // If we have a new sensor value, send to host device
        if (sensor_flag) {
            sensor_flag = 0;
            
            if (is_moving != was_moving) {
                was_moving = is_moving;
                
                // Is this a transition when the value is no longer moving?
                if (!is_moving) {
            
                    // extract miligram value, convert to decigram
                    float eval = ((float)last_value)/100.0;
                    // round decigram
                    eval = rint(eval);
                    // divide by 10, get gram with 1 decimal digit
                    eval /= 10.0;
                    
                    sprintf(sbuf, "%7.1f g", eval);
                    terminal.print(BEGIN_LINE);
                    terminal.print(sbuf);
                    terminal.print(LINE_BREAK);
                }
                // Is this a transition when the value begins moving?
                else {
                    terminal.print(BEGIN_LINE);
                    terminal.print("Wait...");
                    terminal.print(LINE_BREAK);
                }
            }
        }
        
        
        // Serial input and configuration
        if (msg_recv) {
            // Calibration of zero mass
            if ((strcmp(terminal.Command, "CALIBZERO") == 0) && (terminal.NumParams == 0)) {
                calib_zero = last_reading;
                PlaceInEeprom(0, calib_zero);
                SendOK();
            }
            // Calibration of reference mass
            else if ((strcmp(terminal.Command, "CALIBREF") == 0) && (terminal.NumParams == 0)) {
                calib_factor = last_reading;
                PlaceInEeprom(4, calib_factor);
                SendOK();
            }
            // Tare
            else if ((strcmp(terminal.Command, "TARE") == 0) && (terminal.NumParams == 0)) {
                calib_tare = last_reading - calib_zero;
                PlaceInEeprom(8, calib_tare);
                SendOK();
            }
            
            
            else SendError();
            
            msg_recv = 0;
        }
        
        
        
    }
}               